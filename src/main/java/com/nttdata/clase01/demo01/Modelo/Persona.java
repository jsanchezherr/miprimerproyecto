package com.nttdata.clase01.demo01.Modelo;

public class Persona {

    private long id;
    private String nombre;
    private String apellido;


    public long getId() {
        return id;
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
